module.exports = function (plop) {
	plop.setGenerator('body-builder', {
		description: 'This generates a set of templates for that Body field.',
		prompts: [
			{
				type: "list",
				name: "module",
				message: "Which would you like a generate?",
				choices: [
					"New Body",
					"Table",
					"Gallery",
					"Form"
				],
				default: 0
			}
		],
		actions: function(data) {
			var actions = [];

			if (data.module == 'New Body') {
				actions.push({
					type: 'add',
					path: 'templates/_body/body.html',
					templateFile: 'plop-templates/body/body.html'
				});
				actions.push({
					type: 'add',
					path: 'templates/_body/text.html',
					templateFile: 'plop-templates/body/text.html'
				});
				actions.push({
					type: 'add',
					path: 'templates/_body/image.html',
					templateFile: 'plop-templates/body/image.html'
				});
				actions.push({
					type: 'add',
					path: 'templates/_body/embed.html',
					templateFile: 'plop-templates/body/embed.html'
				});
			} else {
				actions.push({
					type: 'add',
					path: 'templates/_body/{{camelCase module}}.html',
					templateFile: 'plop-templates/body/{{camelCase module}}.html'
				});
			}

			return actions;
		}
	});

	plop.setGenerator('font-awesome', {
		description: 'Adds font awesome to your site.scss file.',
		prompts: [],
		actions: function() {
			var fontawesome = require('./node_modules/font-awesome/package.json');
			return [{
				type: 'modify',
				path: 'src/site.scss',
				pattern: /(\/\/ Libraries)/,
				template: '$1\n$fa-font-path: "//netdna.bootstrapcdn.com/font-awesome/'+fontawesome.version+'/fonts";\n@import "font-awesome/scss/font-awesome";'
			}]
		}
	});

	plop.setGenerator('css-grid', {
		description: 'Adds some column grid boilerplate.',
		prompts: [],
		actions: [{
			type: 'modify',
			path: 'src/site.scss',
			pattern: /(\/\/ Custom CSS)/,
			templateFile: 'plop-templates/css/grid.scss'
		}]
	});

	plop.setGenerator('css-start-utility', {
		description: 'Build a website using the utility framework.',
		prompts: [],
		actions: [{
			type: 'add',
			path: 'src/site.scss',
			templateFile: 'plop-templates/css/start-utility.scss'
		}]
	});

	plop.setGenerator('css-start-component', {
		description: 'Build a website using the utility framework.',
		prompts: [],
		actions: [{
			type: 'add',
			path: 'src/site.scss',
			templateFile: 'plop-templates/css/start-component.scss'
		}]
	});
};